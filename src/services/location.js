import axios from 'axios'

export function search(text) {
  return axios.get(`https://api.teleport.org/api/cities/?search=${text}`)
}

export function location(url) {
  return new Promise(async (resolve, reject) => {
    if(!url) { 
      reject() 
    }

    try {
      const { data: city } = await axios.get(url)
      
      const { data: image } = await axios.get(`${city._links['city:urban_area'].href}images`)
      console.log('CITY DATA', city, image)
      resolve({
        full_name: city.full_name,
        location: {
          latitude: city.location.latlon.latitude,
          longitude: city.location.latlon.longitude
        },
        name: city.name,
        image: image.photos[0].image.web
      })
    } catch(e) {
      console.error('Fucking Oops... shit\'s broke', e)
      reject()
    }
  })
}