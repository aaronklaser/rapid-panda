// Leander geo https://api.weather.gov/points/30.5698%2C-97.9254
// Leander forcast https://api.weather.gov/gridpoints/EWX/148,103/forecast
// Leander hourly forcast https://api.weather.gov/gridpoints/EWX/148,103/forecast/hourly

import axios from 'axios'

export function location(latitude = 30.5698, longitude = -97.9254) {
  return axios.get(`https://api.weather.gov/points/${latitude},${longitude}`)
}

export function forcast(url = 'https://api.weather.gov/gridpoints/EWX/148,103/forecast') {
  return axios.get(url)
}

export function hourly(url = 'https://api.weather.gov/gridpoints/EWX/148,103/forecast/hourly') {
  return axios.get(url)
}