import React from 'react'
import {Provider} from 'react-redux'
import styled from 'styled-components'
import {BrowserRouter as Router} from "react-router-dom"

import store from './Store'
import Pages from './app/Pages'
import Header from './app/Header'

export const AppStyles = styled.div`
  flex-grow: 1;
`

function App() {
  return (
    <Provider store={store}>
      <Router>
        <AppStyles>
          <Header />
          <Pages />
        </AppStyles>
      </Router>
    </Provider>
  )
}

export default App
