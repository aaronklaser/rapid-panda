import React from 'react'
import { Switch, Route } from 'react-router-dom'
import {Container} from '@material-ui/core'
import styled from 'styled-components'

import routes from './routes'

export const ContentStyles = styled(Container)`
  padding-top: 20px;
`

function Pages() {
  return (
    <ContentStyles maxWidth="lg">
      <Switch>
        {routes.map(route => <Route key={route.label} {...route} />)}
      </Switch>
    </ContentStyles>
  )
}

export default Pages