import React from 'react'
import {
  Typography
} from '@material-ui/core'

import jsonTest from './jsonTest'

function Home() {
  return (
    <div>
      <div>Home</div>
      <Typography variant="h2">{jsonTest.title}</Typography>
      <Typography variant="body1">{jsonTest.description}</Typography>
    </div>
  )
}

export default Home