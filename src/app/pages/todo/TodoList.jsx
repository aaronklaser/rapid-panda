import React from 'react'
import { connect } from 'react-redux'
import {
  List
} from '@material-ui/core'

import TodoItem from './todoList/TodoItem'
import store from '../../../Store'

const filteredTodos = store.select.todos.filteredTodos

function TodoList({
  todos,
  toggleTodo
}) {
  return (
    <List>
      {todos.map(todo => (
        <TodoItem key={todo.id} {...todo} toggleTodo={() => toggleTodo(todo.id)} />
      ))}
    </List>
  )
}

const mapStateToProps = state => ({
  todos: filteredTodos(state)
})

const mapDispatchToProps = ({ todos: { toggleTodo } }) => ({
  toggleTodo
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)