import React from 'react'
import { connect } from 'react-redux'
import {
  Button
 } from '@material-ui/core'

function FilterLink({ filter, active, setVisibilityFilter, children }) {
  return (
    <Button variant={active ? 'contained' : ''} size="small" color="primary" onClick={() => setVisibilityFilter(filter)}>
      { children }
    </Button>
  )
}

const mapStateToProps = ({ todoFilter }, ownProps) => ({
  active: todoFilter === ownProps.filter
})

const mapDispatchToProps = ({ todoFilter: { setVisibilityFilter } }) => ({
  setVisibilityFilter
})

export default connect(mapStateToProps, mapDispatchToProps)(FilterLink)