import React from 'react'
import {
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Checkbox
} from '@material-ui/core'

function TodoItem({ id, text, completed, toggleTodo }) {
  const labelId = `checkbox-list-secondary-label-${id}`

  return (
    <ListItem key={id} button onClick={toggleTodo}>
      <ListItemText id={labelId} primary={text} />
      <ListItemSecondaryAction>
        <Checkbox
          edge="end"
          onChange={toggleTodo}
          checked={completed}
          inputProps={{ 'aria-labelledby': labelId }}
        />
      </ListItemSecondaryAction>
    </ListItem>
  )
}

export default TodoItem