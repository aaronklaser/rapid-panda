import React from 'react'
import {
  InputBase,
  Divider,
  IconButton
} from '@material-ui/core'
import { 
  AddBox
} from '@material-ui/icons'
import styled from 'styled-components'
import { connect } from 'react-redux'

const AddTodoStyles = styled.div`
  .root {
    padding: 2px 4px;
    display: flex;
    align-items: center;
  }

  .input {
    margin-left: 10px; /* replace with gutter-left */
    flex: 1;
  }

  .iconButton {
    padding: 10px;
  }

  .divider {
    height: 28px;
    margin: 4px;
  }
`

function AddTodo({ addTodo }) {

  const [value, setValue] = React.useState('');

  const handleChange = event => {
    setValue(event.target.value)
  }

  const onSubmit = e => {
    e.preventDefault()
  
    if (!value.trim()) {
      return
    }

    addTodo(value)
    setValue('')
  }

  return (
    <AddTodoStyles>
      <form className="root" onSubmit={onSubmit}>
        <InputBase
          className="input"
          placeholder="Add new task"
          inputProps={{ 'aria-label': 'add new task' }}
          value={value}
          onChange={handleChange}
        />
        <Divider className="divider" orientation="vertical" />
        <IconButton type="submit" color="primary" className="iconButton">
          <AddBox />
        </IconButton>
      </form>
    </AddTodoStyles>
  )
}

const mapDispatchToProps = ({ todos: { addTodo } }) => ({
  addTodo
})

export default connect(null, mapDispatchToProps)(AddTodo)