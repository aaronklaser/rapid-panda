import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Typography
} from '@material-ui/core'
import styled from 'styled-components'
import {
  Paper,
} from '@material-ui/core'


import Cloak from './shared/Cloak'
import Location from './weather/Location'
import Now from './weather/Now'
import Day from './weather/Day'
import Week from './weather/Week'

// This should be moved some where reusable
import store from '../../Store'

const select = store.select
const WeatherStyles = styled.div`
  .round-corners {
    border-radius: 15px;
    margin-bottom: 40px;
  }
`

function Weather() {
  const hasData = useSelector(select.weather.hasData)
  const loading = useSelector(select.weather.loading)
  const geo = useSelector(select.location.geo)
  const dispatch = useDispatch()

  useEffect(() => {
    let active = true

    if (!geo.latitude || !geo.longitude) {
      return undefined
    }
    
    if (active) {
      dispatch.weather.getForcast(geo)
    }

    return () => {
      active = false
    }
  }, [geo, dispatch])

  return (
    <WeatherStyles>
      <Typography variant="h2" gutterBottom>
        Weather
      </Typography>
      <Location dispatch={dispatch} />
      { hasData && (
        <Cloak loading={loading}> 
          <Paper className="round-corners">
            <Now />
            <Day />
            <Week />
          </Paper>
        </Cloak>
      )}
    </WeatherStyles>
  )
}

export default Weather