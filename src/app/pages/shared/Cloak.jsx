import React from 'react'
import {
  LinearProgress
} from '@material-ui/core'

function Cloak({ loading, children }) {
  return (
    <React.Fragment>
      {
        loading
        ? <LinearProgress />
        : children
      }
    </React.Fragment>
  )
}

export default Cloak