import React from 'react'
import { connect } from 'react-redux'
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
} from '@material-ui/core'
import styled from 'styled-components'

export const CounterStyles = styled.div`
  display: flex;
  justify-content: center;
`

const Counter = props => (
  <CounterStyles>
    <Card>      
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          Counter
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          The count is {props.count}
        </Typography>
      </CardContent> 
      <CardActions> 
        <Button size="small" color="primary" onClick={props.increment}>
          Increment
        </Button>
        <Button size="small" color="primary" onClick={props.incrementAsync}>
          Async Increment
        </Button>
      </CardActions>
    </Card>      
  </CounterStyles>
)

const mapState = state => ({
    count: state.count,
})

const mapDispatch = ({ count: { increment, incrementAsync } }) => ({
    increment: () => increment(1),
    incrementAsync: () => incrementAsync(1),
})

export default connect(
    mapState,
    mapDispatch
)(Counter)

