import React from 'react'
import {
  Card,
  CardActions,
  Divider
} from '@material-ui/core'
import styled from 'styled-components'

import AddTodo from './todo/AddTodo'
import TodoList from './todo/TodoList'
import Footer from './todo/Footer'

const TodoStyles = styled.div`
  display: flex;
  justify-content: center;

  .card {
    flex: 1;
    max-width: 600px;
  }
`

function Todo() {
  return (
    <TodoStyles>
      <Card className="card">
        <AddTodo/>
        <Divider />
        <TodoList />
        <CardActions>
          <Footer />
        </CardActions>
      </Card>
    </TodoStyles>
  )
}

export default Todo