import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import {
  Paper,
  TextField,
} from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import * as locationService from '../../../services/location'

const LocationStyles = styled.div`
  .root {  
    display: flex;
    align-items: center;
    margin-bottom: 15px;
  }

  .input {
    flex: 1;
  }

  .iconButton {
    padding: 10px;
  }

  .divider {
    height: 28px;
    margin: 4px;
  }
`

export const formatOptions = data => data.map(loc => ({
    label: loc.matching_full_name,
    href: loc._links['city:item'].href
  })
)

function Location({ dispatch }) {
  const [inputValue, setInputValue] = useState('')
  const [options, setOptions] = useState([])

  useEffect(() => {
    let active = true

    if (inputValue === '') {
      setOptions([])
      return undefined
    }

    (async () => {
      const { data } = await locationService.search(inputValue)
      
      if (active) {
        setOptions(formatOptions(data._embedded['city:search-results']))
      }
    })()

    return () => {
      active = false
    }
  }, [inputValue])

  const handleInputChange = (_e, value) => {
    setInputValue(value)
  }

  const handleChange = (_e, value) => {
    if(!value) return 

    dispatch.location.getLocation(value.href)
  } 

  return (
    <LocationStyles>
      <Paper className="root">
        <Autocomplete
          id="asynchronous-demo"
          className="input"
          getOptionSelected={(option, value) => option.label === value.label}
          getOptionLabel={option => option.label}
          options={options}
          noOptionsText={inputValue === '' ? 'Start typing a location' : `Oops... nothing found for ${inputValue}`}
          onChange={handleChange}
          onInputChange={handleInputChange}
          renderInput={params => (
            <TextField
              {...params}
              label="Location"
              fullWidth
              variant="outlined"
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                ),
              }}
            />
          )}
        />
      </Paper>
    </LocationStyles>
  )
}

export default Location