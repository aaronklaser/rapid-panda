import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import {
  Paper,
  Typography
} from '@material-ui/core'

import store from '../../../Store'

const select = store.select

const NowStyles = styled.div`
  background-image: url(${props => props.image});
  background-repeat: no-repeat;
  background-position: center bottom;
  background-size: cover;
  background-color: lightBlue;
  display: flex;
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;

  .info {
    padding: 20px;
    margin: 20px;
    display: flex;
    flex-direction: column;
    align-items: center;
    min-width: 200px;
  }

`

function Now() {
  
  const now = useSelector(select.weather.now)
  const info = useSelector(select.location.info)

  return (
    <NowStyles image={info.image}>
      { now && (
        <Paper className="info" elevation={4}>
          <Typography variant="h6" gutterBottom>{info.name}</Typography>
          <Typography variant="h1" component="h2">{now.temperature}</Typography>
          <Typography variant="body2" gutterBottom>{now.shortForecast}</Typography>
          <Typography variant="caption"  gutterBottom>{now.windSpeed} {now.windDirection}</Typography>
        </Paper>
      )}
    </NowStyles>
  )
}

export default Now

/*
{ "full_name": "Leander, Texas, United States", "name": "Leander", "image": "https://d13k13wj6adfdf.cloudfront.net/urban_areas/austin_web-e93a5e75a0.jpg" }
{ "number": 1, "name": "", "startTime": "2020-01-02T15:00:00-06:00", "endTime": "2020-01-02T16:00:00-06:00", "isDaytime": true, "temperature": 64, "temperatureUnit": "F", "temperatureTrend": null, "windSpeed": "15 mph", "windDirection": "SSW", "icon": "https://api.weather.gov/icons/land/day/ovc?size=small", "shortForecast": "Cloudy", "detailedForecast": "" }
*/