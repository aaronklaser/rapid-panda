import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'

import store from '../../../Store'
import {
  Typography
} from '@material-ui/core'
import moment from 'moment'

const select = store.select

const DayStyles = styled.div`
  padding: 20px;
  padding-right: 25px;
  
  .today {
    margin-bottom: 20px;
  }

  .detailedForecast {
    margin-top: 20px;
    color: #115293;
  }

  .chart {
    display: flex;
    align-items: flex-end;

    .hourItem {
      flex: 24;
      text-align: center;

      .time {
        margin-right: 5px;
      }
    }
  }
`

const getHeigth = (temp, highest, lowest, max, min) => ((((temp - lowest) * 100) / ((highest - lowest) * (max - min))) * (max - min)) + min

const Hour = styled.div`
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  margin-right: 5px;
  background-color: ${props => props.isDaytime ? '#0dce6c' : '#2b58bd'}
  color: ${props => props.isDaytime ? '#000000' : '#ffffff'}
  text-align: center;
  max-height: 100px;
  min-height: 30px;
  height: ${props => `${getHeigth(props.temperature, props.highest, props.lowest, 100, 30)}px`}
`

function Day() {

  const { hourly, highest, lowest } = useSelector(select.weather.hourly)
  const today = useSelector(select.weather.today)
  
  return (
    <DayStyles>
      { today && (
        <div className="today">
          <Typography variant="h5">{today.name}</Typography>
          <Typography variant="caption">{today.shortForecast}</Typography>
        </div>
      )}
      { hourly.length > 0 && (
        <div className="chart">
          { hourly.map(hour => (
            <div key={hour.startTime} className="hourItem">
              <Hour {...hour} highest={highest} lowest={lowest}>
                <Typography variant="h6">{hour.temperature}</Typography>
              </Hour>
              <Typography variant="caption">{moment(hour.startTime).format("ha")}</Typography>
            </div>
          ))}
        </div>
      )} 
      { today && (
        <div className="detailedForecast">
          <Typography variant="body2">{today.detailedForecast}</Typography>
        </div>
      )} 
    </DayStyles>
  )
}

export default Day