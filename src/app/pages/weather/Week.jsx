import React from 'react'
import styled from 'styled-components'
import { 
  Grid,
  Paper,
  Typography
} from '@material-ui/core'
import {useSelector} from 'react-redux'
import store from '../../../Store'
import moment from 'moment'

const select = store.select

const WeekStyles = styled.div`
  padding: 20px;

  .root {
    flexGrow: 1;
  }

  .week {
    margin-bottom: 20px;
  }

  .paper {
    display: block;
    padding: 15px;
    ${'' /* margin: 20px; */}
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 180px;

    img {
      border-radius: 50%;
      margin: 10px;
    }

    .temp {
      display: flex;
      flex-direction: column;
      align-items: center;
    }
  }
`

function Week() {

  const forcast = useSelector(select.weather.forcast)

  return (
    <WeekStyles>
      {forcast.length > 0 && (
        <div>
          <div className="week">
            <Typography variant="h5">This Week</Typography>
          </div>
          <Grid container justify="center" spacing={2}>
            {forcast.slice(1, 6).map(([day, night]) => (
              <Grid key={day.startTime} item>
                <Paper className="paper" elevation={2}>
                  <Typography variant="h5" styles={{ lineHeight: 1.1 }}>{moment(day.startTime).format('ddd')}</Typography>
                  <Typography variant="caption">{moment(day.startTime).format('Do')}</Typography>
                  
                  <img src={day.icon} alt="" />
                  
                  <Typography variant="body1" component="h2">{day.shortForecast}</Typography>
                  
                  <Grid container className="root" justify="center" spacing={5}>
                    <Grid item className="temp">
                      <Typography variant="h6">{day.temperature}</Typography>
                      <Typography variant="caption" gutterBottom >High</Typography>
                    </Grid> 
                    <Grid item className="temp">
                      <Typography variant="h6">{night.temperature}</Typography>
                      <Typography variant="caption" gutterBottom>Low</Typography>
                    </Grid>
                  </Grid> 
                </Paper>
              </Grid>
            ))}
          </Grid>
        </div>
      )}
    </WeekStyles>
  )
}

export default Week
