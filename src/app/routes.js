import Home from './pages/Home'
import Counter from './pages/Counter'
import Todo from './pages/Todo'
import Weather from './pages/Weather'

const routes = [
  {
    label: 'Home', 
    path: '/', 
    component: Home, 
    exact: true
  },
  {
    label: 'Counter', 
    path: '/counter', 
    component: Counter, 
    exact: true
  },
  {
    label: 'Todo', 
    path: '/todo', 
    component: Todo, 
    exact: true
  },
  {
    label: 'Weather', 
    path: '/weather', 
    component: Weather, 
    exact: true
  },
]

export default routes