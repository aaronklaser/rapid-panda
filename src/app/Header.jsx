import React from 'react'
import styled from 'styled-components'
import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Button
} from '@material-ui/core'
import {Menu} from '@material-ui/icons'
import {NavLink} from "react-router-dom"

import routes from './routes'

export const HeaderStyles = styled.div`
  .menu-button {
    margin-right: 20px;
  }

  .title {
    flex-grow: 1;
  }
`

function Header() {
  return (
    <HeaderStyles>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className="menu-button" color="inherit" aria-label="menu">
            <Menu />
          </IconButton>
          <Typography variant="h6" className="title">
            Rapid Panda
          </Typography>
          {routes.map(route => <Button key={route.label} component={NavLink} to={route.path} color="inherit">{route.label}</Button>)}
        </Toolbar>
      </AppBar>
    </HeaderStyles>
  )
}

export default Header
