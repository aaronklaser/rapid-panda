import * as locationServices from '../services/location'

const location = {
  state: {
    loading: false,
    full_name: '',
    location: {
      latitude: null,
      longitude: null
    },
    name: '',
    image: ''
  }, // initial state
  reducers: {
      // handle state changes with pure functions
      getLocation(state) {
          return {
            ...state,
            loading: true
          }
      },
      setLocation(state, payload) {
        return {
          ...state,
          ...payload
        }
      }
  },
  effects: dispatch => ({
      // handle state changes with impure functions.
      // use async/await for async actions
      async getLocation(payload, rootState) {
        try {
          const data = await locationServices.location(payload)
          dispatch.location.setLocation(data)
        }
        catch(e) {
          console.error('ERROR: Launch Error toast', e)
        }
      },
  }),
  selectors: (slice, createSelector) => ({
    geo() {
      return createSelector(
        slice,
        ({ location }) => location 
      )
    },
    info() {
      return createSelector(
        slice,
        ({full_name, name, image}) => ({full_name, name, image})
      )
    },
  }),
}

export default location
