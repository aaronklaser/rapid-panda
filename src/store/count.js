const count = {
  state: 0,
  reducers: {
    increment(state, payload) {
      return state + payload
    },
  },
  sagas: [
    'incrementAsync'
  ]
}

export default count
