
import { put, takeEvery} from 'redux-saga/effects'

const delay = (ms) => new Promise(res => setTimeout(res, ms))

export function* incrementAsync(action) {
  yield delay(1000)
  yield put({ type: 'count/increment', payload: action.payload })
}

export default function* watchIncrementAsync() {
  yield takeEvery('count/incrementAsync', incrementAsync)
}
