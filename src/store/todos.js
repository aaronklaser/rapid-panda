import { VisibilityFilters } from './todoFilter'
let id = 0

const todos = {
  state: [], // initial state
  reducers: {
    // handle state changes with pure functions
    addTodo(state, text) {
      id = id + 1
      return [
        ...state,
        { id, text, completed: false }
      ]
    },
    toggleTodo(state, id) {
      return state.map(todo =>
        todo.id === id 
        ? {...todo, completed: !todo.completed }
        : todo
      )
    }
  },
  selectors: (slice, createSelector) => {
    return {
      filteredTodos() {
        return createSelector(
          slice,
          state => {
            return state.todoFilter
          },
          (todos, todoFilter) => {
            switch (todoFilter) {
              case VisibilityFilters.SHOW_ALL:
                return todos
              case VisibilityFilters.SHOW_COMPLETED:
                return todos.filter(t => t.completed)
              case VisibilityFilters.SHOW_ACTIVE:
                return todos.filter(t => !t.completed)
              default:
                throw new Error('Unknown filter: ' + todoFilter)
            }
          }
        )
      }
    }
  }
}

export default todos