export const VisibilityFilters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_COMPLETED: 'SHOW_COMPLETED',
  SHOW_ACTIVE: 'SHOW_ACTIVE'
}

const todosFilter = {
  state: VisibilityFilters.SHOW_ALL,
  reducers: {
    setVisibilityFilter(_state, filter) {
      return filter
    }
  }
}

export default todosFilter