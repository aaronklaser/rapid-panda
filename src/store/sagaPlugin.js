const sagaPlugin = {
  exposed: {
    sagas: {},
  },
  onModel(model) {
    if (!model.sagas) {
			return
    }

    const sagas =
			typeof model.sagas === 'string'
				? model.sagas(this.dispatch)
				: model.sagas
    
    const regex = /\//

    for (const sagaName of model.sagas) {
			this.validate([
				[
					!!sagaName.match(regex),
					`Invalid Saga name (${model.name}/${sagaName})`
				],
				[
					typeof sagas[sagaName] !== 'string',
					`Invalid Saga (${model.name}/${sagaName}). Must be a String`,
				],
      ])

			this.dispatch[model.name][sagaName] = this.createDispatcher.apply(
				this,
				[model.name, sagaName]
			)

			this.dispatch[model.name][sagaName].isSaga = true
		}
  },
}

export default sagaPlugin
