import { all} from 'redux-saga/effects'

import watchIncrementAsync from './sagas/incrementAsync.saga'

export default function* rootSaga() {
  yield all([
    watchIncrementAsync()
  ])
}