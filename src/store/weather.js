import * as weatherService from '../services/weather'
import * as R from 'ramda'

const weather = {
  state: {
    loading: false,
    hourly: [],
    forcast: []
  }, // initial state
  reducers: {
    // handle state changes with pure functions
    getForcast(state) {
      return {
        ...state,
        loading: true
      }
    },
    setForcast(state, payload) {
      return {
        ...state,
        loading: false,
        ...payload
      }
    },
  },
  effects: dispatch => ({
    // handle state changes with impure functions.
    // use async/await for async actions
    async getForcast({ latitude, longitude }, rootState) {
      try {
        const { data } = await weatherService.location(latitude, longitude)
        const [hourly, forcast] = await Promise.all([
          weatherService.hourly(data.properties.forecastHourly), 
          weatherService.forcast(data.properties.forecast)
        ])
        
        dispatch.weather.setForcast({
          hourly: hourly.data.properties.periods,
          forcast: forcast.data.properties.periods
        })
      }
      catch(e) {
        console.error('ERROR: Launch Error toast', e)
      }
    },
  }),
  selectors: (slice, createSelector) => ({
    now() {
      return createSelector(
        slice,
        ({ hourly }) => hourly[0]
      )
    },
    hourly() {
      return createSelector(
        slice,
        weather => ({
          hourly: weather.hourly.slice(0, 24),
          highest: R.reduce((acc, cur) => cur.temperature > acc ? cur.temperature : acc, -999, weather.hourly),
          lowest: R.reduce((acc, cur) => cur.temperature < acc ? cur.temperature : acc, 999, weather.hourly)
        })
      )
    },
    today() {
      return createSelector(
        slice,
        ({ forcast }) => forcast[0]
      )
    },
    forcast() {
      return createSelector(
        slice,
        weather => Object.values(weather.forcast.reduce((acc, cur, i) => {
          const index = i % 2 === 0 ? i : i - 1
          return {
            ...acc,
            [index]: acc[index] ? [...acc[index], cur] : [cur]
          }
        }, {}))
      )
    },
    loading() {
      return createSelector(
        slice,
        weather => weather.loading
      )
    },
    hasData() {
      return createSelector(
        slice,
        ({ hourly, forcast }) => hourly.length > 0 && forcast.length > 0 
      )
    }
  })
}

export default weather