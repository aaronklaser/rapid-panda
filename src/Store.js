import { init } from '@rematch/core'
import selectPlugin from '@rematch/select'
import createSagaMiddleware from 'redux-saga'

import count from './store/count'
import location from './store/location'
import todos from './store/todos'
import todoFilter from './store/todoFilter'
import weather from './store/weather'

import sagaPlugin from './store/sagaPlugin'

import rootSaga from './store/Sagas'

const sagaMiddleware = createSagaMiddleware()

const store = init({
  redux: {
    middlewares: [sagaMiddleware]
  },
  models: {
    count,
    location,
    todos,
    todoFilter,
    weather,
  },
  plugins: [selectPlugin(), sagaPlugin],
})

sagaMiddleware.run(rootSaga)

export default store
